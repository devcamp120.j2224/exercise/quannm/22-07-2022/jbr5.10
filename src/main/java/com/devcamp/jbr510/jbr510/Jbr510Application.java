package com.devcamp.jbr510.jbr510;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr510Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr510Application.class, args);
	}

}
