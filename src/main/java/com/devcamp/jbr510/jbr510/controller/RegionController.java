package com.devcamp.jbr510.jbr510.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr510.jbr510.model.Region;
import com.devcamp.jbr510.jbr510.model.RegionService;

@RestController
public class RegionController {
    @CrossOrigin
    @GetMapping("/region-info")
    public Region requestRegion(@RequestParam(required =  true, name = "regionCode") String regionCode) {
        ArrayList<Region> listRegion = RegionService.getListRegion();
        Region regionFounded = new Region();
        for (Region region : listRegion) {
            if (region.getRegionCode().equals(regionCode)) {
                regionFounded = region;
            }
        }
        return regionFounded;
    }
}
