package com.devcamp.jbr510.jbr510.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr510.jbr510.model.Country;
import com.devcamp.jbr510.jbr510.model.CountryService;

@RestController
public class CountryController {
    @CrossOrigin
    @GetMapping("/countries")
    public static ArrayList<Country> getListCountry() {
        ArrayList<Country> listCountry = CountryService.getListCountry();
        return listCountry;
    }

    @CrossOrigin
    @GetMapping("/country-info")
    public Country requestCountry(@RequestParam(required =  true, name = "countryCode") String countryCode) {
        ArrayList<Country> listCountry = CountryService.getListCountry();
        Country countryFounded = new Country();
        for (Country country : listCountry) {
            if (country.getCountryCode().equals(countryCode)) {
                countryFounded = country;
            }
        }
        return countryFounded;
    }
}
