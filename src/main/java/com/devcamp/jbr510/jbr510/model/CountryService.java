package com.devcamp.jbr510.jbr510.model;

import java.util.ArrayList;

public class CountryService {
    public static ArrayList<Country> getListCountry() {
        ArrayList<Country> listCountry = new ArrayList<Country>();
        Country vn = new Country("VN", "Việt Nam", RegionService.getListRegionVN());
        Country usa = new Country("USA", "Hoa Kỳ", RegionService.getListRegionUSA());
        Country phap = new Country("FRA", "Pháp", RegionService.getListRegionPhap());
        listCountry.add(vn);
        listCountry.add(usa);
        listCountry.add(phap);
        return listCountry;
    }
}
