package com.devcamp.jbr510.jbr510.model;

import java.util.ArrayList;

public class RegionService {
    
    public static ArrayList<Region> getListRegionVN() {
        ArrayList<Region> regionsVN = new ArrayList<Region>();
        Region hanoi = new Region("HN", "Hà Nội");
        Region sg = new Region("SG", "Sài Gòn");
        regionsVN.add(hanoi);
        regionsVN.add(sg);
        return regionsVN;
    }

    public static ArrayList<Region> getListRegionUSA() {
        ArrayList<Region> regionsUSA = new ArrayList<Region>();
        Region newyork = new Region("NY", "New York");
        Region texas = new Region("TX", "Texas");
        regionsUSA.add(newyork);
        regionsUSA.add(texas);
        return regionsUSA;
    }

    public static ArrayList<Region> getListRegionPhap() {
        ArrayList<Region> regionsPhap = new ArrayList<Region>();
        Region paris = new Region("PR", "Paris");
        Region tou = new Region("TOU", "Toulouse");
        regionsPhap.add(paris);
        regionsPhap.add(tou);
        return regionsPhap;
    }

    public static ArrayList<Region> getListRegion() {
        ArrayList<Region> listRegion = new ArrayList<Region>();
        Region hanoi = new Region("HN", "Hà Nội");
        Region sg = new Region("SG", "Sài Gòn");
        Region newyork = new Region("NY", "New York");
        Region texas = new Region("TX", "Texas");
        Region paris = new Region("PR", "Paris");
        Region tou = new Region("TOU", "Toulouse");
        listRegion.add(hanoi);
        listRegion.add(sg);
        listRegion.add(newyork);
        listRegion.add(texas);
        listRegion.add(paris);
        listRegion.add(tou);
        return listRegion;
    }
}
